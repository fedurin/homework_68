# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
types_list = ['Бар', 'Кафе', 'Кафе-бар', 'Ресторан']
types_list.each do |name|
  Type.create(name: name)
end

categories_list = ['Казахская кухня', 'Китайская кухня', 'Европейская кухня', 'Грузинская кухня', 'Итальянская кухня']
categories_list.each do |category|
  Category.create(name: category)
end

restaurants_list = [
  ['Domino pizza', 'При заказе трех больших пицц четвертая любая пицца Вам в подарок! Выберете любимую пиццу от Домино БЕСПЛАТНО! Внимание: при данной акции не действует акция доставка за 60 минут или пицца бесплатно', 4, '44801.png'],
  ['Кафе Flamingo', 'При заказе с этого заведения вы получаете скидку 10% на всё меню', 2,'56921.png'],
  ['Кафе Бар Limoncello', 'Средний чек 400 сом', 3, '55364.png'],
  ['United Kitchens', 'Тут НОРМАЛЬНО!', 4, '46384.png'],
  ['Zolden beer', 'При заказе из этого заведения действует скидка 10%', 1, '49990.png'],
]

restaurants_list.each_with_index do |r, index|
  index = Restaurant.create(
    name: r[0],
    description: r[1],
    type_id: r[2]
    )
  index.picture.attach(
    io: File.open("public/uploads/restaurants/#{r[3]}"),
    filename: "#{r[3]}"
    )
end

dish_list = [
  ['Манная каша', '300г. Манная крупа,молоко,сливочное масло.', 78, 4, 3, '55173.png'],
  ['Салат Цезарь', '280г. Куриная грудка,крутоны,листья салата айсберг,сыр джугас,соус цезарь.', 99, 4, 3, '55200.png'],
  ['Солярка рыбная', '300гр', 77, 3, 4, '44580.png' ],
  ['Том ям кунг', '300 гр. Тайский острый суп с креветками и курицей', 119, 5, 2, '51955.png'],
  ['Салат Греческий', '300г. Огурцы,помидоры,болгарский перец,фетакса,листья салата,оливковое масло,лимон,маслины.', 200, 2, 3, '55198.png'],
  ['Пицца мясная', 'Говядина рубленая, баранина, фарш, колбаса мусульманская вареная и копченая, соус, сыр Моцарелла, болгарский перец, оливки, орегано, базилик, сыр Голландский.', 170, 1, 5, '44749.png'],
  ['Пицца Американка', 'Соус, сыр, помидоры, колбасные изделия, фарш, окорочок копченый, грибы, болгарский перец, маслины, орегано, базилик', 160, 1, 5, '44750.png'],
  ['Чечевичный суп', '380г. Чечевица,копченная куриная грудка,лук,крутоны,лимон.', 120, 4, 2, '55201.png']
]

dish_list.each_with_index do |d, index|
  index = Dish.create(
    name: d[0],
    description: d[1],
    price: d[2],
    restaurant_id: d[3],
    category_id: d[4]
    )
  index.photo.attach(
    io: File.open("public/uploads/dishes/#{d[5]}"),
    filename: "#{d[5]}"
    )
end
