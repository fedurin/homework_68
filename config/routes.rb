Rails.application.routes.draw do
  devise_for :users
  ActiveAdmin.routes(self)
  root 'restaurants#index'
  delete 'carts/clear' => 'carts#clear'
  resources :restaurants, only: [:index, :show]
  resources :dishes
  resources :carts
  resources :line_items
  resources :types
  resources :categories
end
