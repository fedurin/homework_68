dish_list = [
  ['Манная каща', '300г. Манная крупа,молоко,сливочное масло.', 78, 4, 3, '55173.png'],
  ['Цузарь', '280г. Куриная грудка,крутоны,листья салата айсберг,сыр джугас,соус цезарь.', 99, 4, 3, '55200.png']
]

dish_list.each_with_index do |d, index|
  d.each do |item|
    index = Dish.create(
      name: item[0],
      description: item[1],
      price: item[2],
      restaurant_id: item[3],
      category_id: item[4]
      )
    index.photo.attach(
      io: File.open("public/uploads/dishes/#{item[5]}"),
      filename: '#{item[5]}'
      )
  end
end

class Dish
  def initialize(name, description, price, restaurant_id, category_id)
    @name = name
    @description = description
    @price = price
    @restaurant_id = restaurant_id
    @category_id = category_id
  end
end

p Dish
