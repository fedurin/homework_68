module ApplicationHelper
  def add_dish(dish_id)
    current_dish = line_items.find_by(dish_id: dish_id)
    if current_dish
      current_dish.quantity += 1
    else
      current_dish = line_items.build(dish_id: dish_id)
    end
    current_dish
  end

  def get_category_name(category)
    category = Category.find(category)
    return category.name
  end
end
