module CartsHelper

  def total_price(item)
    @cart = []
    @cart << item.dish.price * item.quantity
    @total_price = item.dish.price * item.quantity
    # p @cart
  end

  def total_price_cart
    Cart.all.to_a.sum { |item| total_price(item)}
  end
end
