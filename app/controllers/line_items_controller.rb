class LineItemsController < ApplicationController
  include CurrentCart
  before_action :set_cart, only: [:create]

  def create
  @restaurant = Restaurant.find(params[:restaurant])
  dish = Dish.find(params[:dish_id])
  @line_item = @cart.add_dish(dish.id)
    if @line_item.save
      flash[:notice] = 'Line item was successfully created.'
      redirect_to @restaurant
    end
  end

end