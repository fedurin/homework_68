class CartsController < ApplicationController
  include CurrentCart
  before_action :set_cart, only: [:create]

  def destroy
    @cart = Cart.find(params[:id])
    @cart.destroy if @cart.id == session[:cart_id]
    session[:cart_id] = nil
    respond_to do |format|
      format.html { redirect_to root_path,
                                notice: 'Теперь ваша корзина пуста!' }
      format.json { head :no_content }
    end
  end

  private
  def cart_params
    params.require(:cart).permit(:dish_id, :quantity)
  end
end
