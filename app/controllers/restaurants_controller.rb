class RestaurantsController < ApplicationController
  include CurrentCart
  before_action :set_cart
  def index
    @restaurants = Restaurant.all
     @types = Type.all
     @categories = Category.all
  end

  def show
    @restaurant = Restaurant.find(params[:id])
    @dishes = @restaurant.dishes
    @types = Type.all
  end
end
