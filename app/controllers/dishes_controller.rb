class DishesController < ApplicationController
  include CurrentCart
  before_action :set_cart, only: [:show]
  def index
    @dishes = Dish.all
  end

  def show
    @dish = Dish.find(params[:id])
  end
end
