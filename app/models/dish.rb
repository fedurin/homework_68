class Dish < ApplicationRecord
  belongs_to :restaurant
  has_one_attached :photo
  # has_many :carts
  has_many :line_items, dependent: :destroy
  belongs_to :category

  before_destroy :ensure_not_referenced_by_any_line_item

  private
  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'существуют товарные позиции')
      return false
    end
  end
end
