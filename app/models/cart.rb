class Cart < ApplicationRecord
  # belongs_to :dish
  has_many :line_items, dependent: :destroy

  def add_dish(dish_id)
    current_dish = line_items.find_by(dish_id: dish_id)
    if current_dish
      current_dish.quantity += 1
    else
      current_dish = line_items.build(dish_id: dish_id)
    end
    current_dish
  end

  def total_price
    line_items.to_a.sum { |item| item.total_price }
  end
end
