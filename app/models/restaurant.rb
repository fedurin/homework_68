class Restaurant < ApplicationRecord
  has_many :dishes
  has_one_attached :picture
  belongs_to :type
end
